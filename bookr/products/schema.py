import graphene
from graphene_django import DjangoObjectType
from . import models



class ProductType(DjangoObjectType):
    class Meta:
        model = models.Product
        fields = "__all__"


class Query(graphene.ObjectType):
    all_products = graphene.List(ProductType)
    product = graphene.Field(ProductType, product_id=graphene.Int())

    def resolve_all_products(self, info, **kwargs):
        return models.Product.objects.all()

    def resolve_product(self, info, product_id):
        return models.Product.objects.get(pk=product_id)

class ProductInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    description = graphene.String()
    price = graphene.Int() 
    quantity= graphene.Int()


class CreateProduct(graphene.Mutation):
    class Arguments:
        product_data = ProductInput(required=True)

    product = graphene.Field(ProductType)

    @staticmethod
    def mutate(root, info, product_data=None):
        product_instance = Product( 
            title=product_data.title,
            author=product_data.author,
            year_published=product_data.year_published,
            review=product_data.review
        )
        product_instance.save()
        return CreateProduct(product=product_instance)


class Mutation(graphene.ObjectType):
    create_product = CreateProduct.Field()
    

schema = graphene.Schema(query=Query, mutation=Mutation)
